package com.przemek.carproject.controller;

import com.przemek.carproject.cars.Cars;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by RENT on 2017-06-30.
 */
@RestController
public class CarsController {

    List<Cars> cars = Arrays.asList(
            new Cars("Laguna", 2000, 1800, 98, "ASDFASDF2344SF4"),
            new Cars("Audi", 2000, 2800, 198, "ASDFASSDF334SF4"),
            new Cars("Mazda", 2004, 1900, 128, "ASDFAS33FWR44SF4"),
            new Cars("Polonez", 1990, 1500, 68, "AS24433SDF334SF4"),
            new Cars("Fiat", 1999, 1600, 70, "AS24433S34RR4SF4")
    );

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Cars> carList(){
        return cars;
    }

    @RequestMapping(value = "/add", method = RequestMethod.PUT)
    public Cars addCar(@RequestBody Cars car){
        cars.add(car);
        return car;
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
    public Cars editCar(@PathVariable("id") String id, @RequestBody Cars car ){
        List<Cars> carList = cars.stream().filter(x -> x.getId().equalsIgnoreCase(id)).collect(Collectors.toList());
        if (carList.size() == 1){
            carList.get(Integer.parseInt(id)).setModel(car.getModel());
            carList.get(Integer.parseInt(id)).setYear(car.getYear());
            carList.get(Integer.parseInt(id)).setCcm(car.getCcm());
            carList.get(Integer.parseInt(id)).setPh(car.getPh());
            carList.get(Integer.parseInt(id)).setVin(car.getVin());
        } else {
            throw new RuntimeException("Nie wiem co edytować!");
        }
        return carList.get(Integer.parseInt(id));
    }

}
