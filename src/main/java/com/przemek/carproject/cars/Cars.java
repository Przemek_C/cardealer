package com.przemek.carproject.cars;

import java.util.UUID;

/**
 * Created by RENT on 2017-06-30.
 */
public class Cars {
    private String id;
    private String model;
    private int year;
    private int ccm;
    private int ph;
    private String vin;

    public Cars(String model, int year, int ccm, int ph, String vin) {
        this.id = UUID.randomUUID().toString();
        this.model = model;
        this.year = year;
        this.ccm = ccm;
        this.ph = ph;
        this.vin = vin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getCcm() {
        return ccm;
    }

    public void setCcm(int ccm) {
        this.ccm = ccm;
    }

    public int getPh() {
        return ph;
    }

    public void setPh(int ph) {
        this.ph = ph;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}
